#ifndef RECTTABLEGEN_H
#define RECTTABLEGEN_H

#include "Rect.h"

#include <list>
#include <string>

namespace SiberSystemsInc
{

class RectTableColumn
{
public:
  RectTableColumn(int pos = 0, int width = 0)
  {
    this->pos = pos;
    this->width = width;
  }

  int pos;
  int width;

  bool operator==(const RectTableColumn& other) const
  {
    return (width == other.width)||(pos == other.pos);
  }

  bool operator!=(const RectTableColumn& other) const
  {
    return (width != other.width)||(pos != other.pos);
  }
};

class RectTableRow
{
public:
  RectTableRow(int pos = 0, int height = 0)
  {
    this->pos = pos;
    this->height = height;
  }

  int pos = 0;
  int height = 0;

  bool operator==(const RectTableRow& other) const
  {
    return (height == other.height)&&(pos == other.pos);
  }

  bool operator!=(const RectTableRow& other) const
  {
    return (height != other.height)||(pos != other.pos);
  }
};

class RectTableRect
{
public:
  RectTableRect(int rowIdx = 0, int rowCount = 0, int colIdx = 0, int colCount = 0, int color = 0x888888)
  {
    this->rowIdx = rowIdx;
    this->rowCount = rowCount;
    this->colIdx = colIdx;
    this->colCount = colCount;
    this->color = color;
    UserDataPtr = nullptr;
  }

  int color;
  int rowIdx, colIdx;
  int rowCount, colCount;

  void * UserDataPtr;

  int lastColIdx(){return colIdx + colCount - 1;}
  int lastRowIdx(){return rowIdx + rowCount - 1;}

  bool operator==(const RectTableRect& other) const
  {
    return (rowIdx == other.rowIdx)&&(rowCount == other.rowCount)&&(colIdx == other.colIdx)&&(color == other.color);
  }

  bool operator!=(const RectTableRect& other) const
  {
    return (rowIdx != other.rowIdx)||(rowCount != other.rowCount)||(colIdx != other.colIdx)||(color != other.color);
  }
};

class RectTable
{
public:
  std::list<RectTableColumn> columns;
  std::list<RectTableRow> rows;
  std::list<RectTableRect> rects;

  bool operator==(RectTable& other) const
  {
    return (columns == other.columns)&&(rows == other.rows)&&(rects == other.rects);
  }
};

class RectTableGen
{
public:
  static std::string rects_to_html(std::list<Rect> * rects, int border = 0, int cellpadding = 0, int cellspacing = 0);
  static void rects_to_html_file(const char* fileName, std::list<Rect>* rects, int border = 0, int cellpadding = 0, int cellspacing = 0);

private:
  RectTableGen(){rects = 0;}

  std::list<Rect> * rects;
  SortedRectList rectsSorted[2][2]; // для каждой оси и для каждой точки в отрезке
  RectTable table;

  void sort_rects_by(Axis axis);
  void sort_rects_by(Axis axis, RectAxisPointType pointType);

  void table_gen_proc_axis(Axis axis);
  void table_gen_proc_data(Axis axis, SortedRectsIterator::IteratorData* iD, int val0, int length, int* I);

  void load_rects(std::list<Rect> * rects);
  RectTable* table_gen();
  std::string table_to_html(int border = 0, int cellpadding = 0, int cellspacing = 0);

#ifdef QT_TESTLIB_LIB
  friend class TestRectTableGen;
#endif
};

} // SiberSistemsInc

#endif // RECTTABLEGEN_H
